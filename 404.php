<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */
get_header(); ?>

<div class="page404-1 page-404-show">

    <div class="page404-1__desc">
        <img src="https://magazyndigital.pl/wp-content/uploads/2022/02/404-element.png" alt="">
        <!-- <span>Nie znaleziono strony</span> -->
    </div>

</div>

<div class="page404-2">

    <div class="page404-1__desc">
        <img src="https://magazyndigital.pl/wp-content/uploads/2022/02/404-element2.png" alt="">
        <!-- <span>Nie znaleziono strony</span> -->
    </div>

</div>

<style>
    .page404-1__desc {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    
}

.error404 #page .page404-1, .error404 #page .page404-2 {
    overflow: hidden !important;
}

html {
    overflow: hidden;
}

.page404-2 img {
    max-width: 520rem;
}

.page404-1 img {
    max-width: 520rem;
}

@media(max-width: 992px){
   .page404-2 img {
    max-width: 300rem;
}

.page404-1 img {
    max-width: 300rem;
}
 
}
</style>

<script>
setInterval(function() {

    jQuery('.page404-1').fadeToggle('page-404-show');
    jQuery('.page404-2').fadeToggle('page-404-show');

}, 5000);

</script>

<?php  