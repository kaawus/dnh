<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */
 
?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>

<?php get_template_part( 'footer-widget' ); ?>
<footer id="footer-contact" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
    <div class="container-fluid">
        <div class="row align-items-lg-center">
            <div class="col-lg-6 text-center text-lg-left">
                <img src="/wp-content/uploads/2022/01/Kontakt-kopia.png" class="text-kontakt" alt="">
            </div>
            <div class="col-lg-6">
                <div class="footer-contact text-center text-lg-right">
                    <div class="logo-footer"><img src="/wp-content/uploads/2022/01/Inteligentny-obiekt-wektorowy.svg"
                            alt=""></div>
                    <div class="magazyn text-uppercase">Digital Now! Holding</div>
                    <div class="yellow">
                        Digital Now! Holding Sp. z o.o.<br>
                        Ul. Polska 114<br>
                        60-401 Poznań
                    </div>
                    <div class="mail"><a href="https://digitalnow-holding.com/">digitalnow-holding.com</a></div>
                    <div class="mail"><a href="mailto:redakcja@magazyndigital.pl">e-mail: redakcja@magazyndigital.pl</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
<?php endif; ?>

<div class="newsletter-success">
    <div class="newsletter-success__wrapper">
        Dziękujemy
        <span>za zapisanie się
            do newslettera</span>
        <img src="/wp-content/uploads/2022/01/koperta.png" alt="">
        <div class="newsletter-success__wrapper--close">
            <img src="/wp-content/uploads/2022/01/zamknij.png" alt="">
        </div>
    </div>

</div>

<div class="reklama-success">
    <div class="reklama-success__wrapper">
        Dziękujemy
        <span>Twoja wiadomośc trafiła już do skrzynki redakcji Magazyn Digital ;)<br>Wrócimy do Ciebie z odpowiedzą tak szybko, jak to możliwe.</span>
        <img src="/wp-content/uploads/2022/01/koperta.png" alt="">
        <div class="reklama-success__wrapper--close">
            <img src="/wp-content/uploads/2022/01/zamknij.png" alt="">
        </div>
    </div>

</div>


</div><!-- #page -->

<?php wp_footer(); ?>
</body>

</html>