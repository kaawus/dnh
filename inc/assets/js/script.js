jQuery(function($) {

    // Add custom script here. Please backup the file before editing/modifying. Thanks

    // Run the script once the document is ready
    $(document).ready(function() {
        if (window.location.href.indexOf("pisza-dla-nas") > -1) {
            setTimeout(function() {
                var hash = window.location.hash;
                var offsetleft1 = $('.osoby .container-fluid').offset();
                var offsetleft2 = $(hash).offset();
                var offsettransX = -offsetleft2.left + 15;
                $(hash).toggleClass('osoba-single-active');
                $(hash).parent().find(".desc-osoba").css("transform", 'translateX(' + offsettransX + 'px)');
                $(hash).parent().find(".desc-osoba").slideDown();
            }, 1000);
        }
        /*
                setTimeout(function () {
                    if(window.location.href === "https://magazyndigital.pl/pisza-dla-nas/#kamil-gorecki"){
                        console.log("kamil-gorecki");
                        var offsetleft1 = $('.osoby .container-fluid').offset();
                        console.log(offsetleft1.left );
                        var offsetleft2 = $('#kamil-gorecki').offset();
                        console.log(offsetleft2.left);
                        //console.log(offsetleft1.left + 15 - offsetleft2.left)
                        var offsettransX =  - offsetleft2.left + 15;
                        console.log(offsettransX);
                        //$(".desc-osoba").hide();
                            $('#kamil-gorecki').toggleClass('osoba-single-active');
                        $('#kamil-gorecki').parent().find(".desc-osoba").css("transform", 'translateX('+ offsettransX +'px)');
                        $('#kamil-gorecki').parent().find(".desc-osoba").slideDown();
                    }
                }, 1500);

                setTimeout(function () {
                    if(window.location.href === "https://magazyndigital.pl/pisza-dla-nas/#michal-kulis"){
                        console.log("michal-kulis");
                        var offsetleft1 = $('.osoby .container-fluid').offset();
                        console.log(offsetleft1.left );
                        var offsetleft2 = $('#michal-kulis').offset();
                        console.log(offsetleft2.left);
                        //console.log(offsetleft1.left + 15 - offsetleft2.left)
                        var offsettransX =  - offsetleft2.left + 15;
                        console.log(offsettransX);
                        //$(".desc-osoba").hide();
                            $('#michal-kulis').toggleClass('osoba-single-active');
                        $('#michal-kulis').parent().find(".desc-osoba").css("transform", 'translateX('+ offsettransX +'px)');
                        $('#michal-kulis').parent().find(".desc-osoba").slideDown();
                    }
                }, 1500);
        */


        $(window).scroll(function() {
            if ($(this).scrollTop() > 10) {
                $('header#hamburger').addClass("scrollheader");
            } else {
                $('header#hamburger').removeClass("scrollheader");
            }
        });

        document.addEventListener('wpcf7mailsent', function(event) {
            if ('23' == event.detail.contactFormId) { // Sends sumissions on form 947 to the first thank you page
                $('.newsletter-success').fadeIn();
            }
        }, false);

        document.addEventListener('wpcf7mailsent', function(event) {
            if ('438' == event.detail.contactFormId) { // Sends sumissions on form 947 to the first thank you page
                $('.newsletter-success').fadeIn();
            }
        }, false);

        document.addEventListener('wpcf7mailsent', function(event) {
            if ('446' == event.detail.contactFormId) { // Sends sumissions on form 947 to the first thank you page
                $('.newsletter-success').fadeIn();
            }
        }, false);
        document.addEventListener('wpcf7mailsent', function(event) {
            if ('697' == event.detail.contactFormId) { // Sends sumissions on form 947 to the first thank you page
                $('.reklama-success').fadeIn();
            }
        }, false);


        $('.newsletter-success__wrapper--close').click(function() {
            $('.newsletter-success').fadeOut();
        });
        $('.reklama-success__wrapper--close').click(function() {
            $('.reklama-success').fadeOut();
        });

        $('#nav-icon3').click(function() {
            $(this).toggleClass('open');
            $('.navbar').fadeToggle();
        });

        if ($(window).width() < 1024) {
            $('header#hamburger .navbar-nav li.no-active a').click(function() {
                $('#nav-icon3').removeClass('open');
                $('.navbar').fadeOut();
            });
        } else {
            $('header#hamburger .navbar-nav li.no-active a').click(function() {
                // $('#nav-icon3').removeClass('open');
                // $('.navbar').fadeIn();
            });
        }

        $(window).resize(function() {
            if ($(window).width() < 1024) {
                $('header#hamburger .navbar-nav li.no-active a').click(function() {
                    $('#nav-icon3').removeClass('open');
                    $('.navbar').fadeOut();
                });
            } else {
                $('header#hamburger .navbar-nav li.no-active a').click(function() {
                    // $('#nav-icon3').removeClass('open');
                    // $('.navbar').fadeIn();
                });
            }
        })

        /*
                var swiper = new Swiper(".mySwiperBanner", {
                    slidesPerView: 1,
                    spaceBetween: 0,
                    
                    navigation: {
                        nextEl: ".swiper-next",
                        prevEl: ".swiper-prev",
                    },
                });*/
        var swiper = new Swiper(".mySwiperMagazyn", {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            navigation: {
                nextEl: ".swiper-next-magazyn",
                prevEl: ".swiper-prev-magazyn",
            },
        });
        var swiper = new Swiper(".mySwiperNews", {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                },
            },
        });
        var swiper2 = new Swiper(".mySwiperLogo", {
            slidesPerView: 2,
            spaceBetween: 20,
            loop: true,
            autoplay: {
                delay: 1500,
                disableOnInteraction: false,
            },
            breakpoints: {
                480: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                },
                640: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                },
                992: {
                    slidesPerView: 5,
                    spaceBetween: 0,
                },
                1199: {
                    slidesPerView: 7,
                    spaceBetween: 0,
                },
            },
        });





        /*
        $('.page-template-pisza-o-nas').click(function(){
            $('.desc-osoba').slideUp();
        });
        */

        $('.osoba-single').click(function() {

            if ($(this).hasClass("osoba-single-active")) {
                $(this).parent().find(".desc-osoba").hide();
                $(this).removeClass("osoba-single-active");
                $(this).parent().find(".desc-osoba").css("opacity", "0");
            } else {
                //$('.desc-osoba').parent().find(".desc-osoba").removeClass('d-none');
                $('.osoba-single-active').parent().find(".desc-osoba").slideUp();
                $('.osoba-single-active').parent().find(".desc-osoba").css("opacity", "0")
                $('.osoba-single-active').removeClass("osoba-single-active");
                //$('.desc-osoba').slideToggle();
                $(this).toggleClass('osoba-single-active');

                var offsetleft1 = $('.osoby .container-fluid').offset();
                console.log(offsetleft1.left);
                var offsetleft2 = $(this).offset();
                console.log(offsetleft2.left);
                //console.log(offsetleft1.left + 15 - offsetleft2.left)
                var offsettransX = -offsetleft2.left + 15;
                console.log(offsettransX);
                //$(".desc-osoba").hide();
                $(this).parent().find(".desc-osoba").css("transform", 'translateX(' + offsettransX + 'px)');

                $(this).parent().find(".desc-osoba").css("opacity", "1")

                $(this).parent().find(".desc-osoba").slideDown();

                //$(this).append('<div style="position:absolute; bottom: -50rem; left: 0; width: 1000rem; height: 200rem; background:red;">TEST</div>')
            }


        });






        $('.page-template-czytaj-online .magazyn .cta-form').click(function() {
            $(this).slideUp();
            $(this).parent().parent().find(".download-magazine").slideDown();
        });

        $('.page-template-czytaj-online .magazyn .cta-form').click(function() {
            $(this).slideUp();
            $(this).parent().parent().find(".download-magazine").slideDown();
        });

        $('.pokaz-caly-spis').click(function() {
            $(this).parent().find('.hidden-pojedyncze').slideDown();
            $(this).fadeOut();
            $(this).parent().find('.chowaj-caly-spis').fadeIn();
        });

        $('.chowaj-caly-spis').click(function() {
            $(this).parent().find('.hidden-pojedyncze').slideUp();
            $(this).fadeOut();
            $(this).parent().find('.pokaz-caly-spis').fadeIn();
        });

        $('.buy-section__right--cta1').click(function() {
            $('.buy-section__right--title2').fadeIn();
            $('.buy-section__right--title1').fadeOut();
            $('.buy-section__right--cta1').fadeOut();
            $('.buy-section__right img').css("mix-blend-mode", "normal");
        });

        $('.buy-section__left--cta1').click(function() {
            $('.buy-section__left--title2').fadeIn();
            $('.buy-section__left--title1').fadeOut();
            $('.buy-section__left--cta1').fadeOut();
            $('.buy-section__left img').css("mix-blend-mode", "normal");
        });

        $('.buy-section__right--cta2').click(function() {
            $('.kup-magazyn__form-prenumerata2').fadeIn();
            $('html').css({
                'overflow-y': 'hidden'
            });
        });
        $('.kup-magazyn__form-prenumerata2 .form-zamow-body-close').click(function() {
            $('.kup-magazyn__form-prenumerata2').fadeOut();
            $('html').css({
                'overflow-y': 'scroll'
            });
        });
        $('.buy-section__left--cta2').click(function() {
            $('.kup-magazyn__form-prenumerata').fadeIn();
            $('html').css({
                'overflow-y': 'hidden'
            });
        });
        $('.kup-magazyn__form-prenumerata .form-zamow-body-close').click(function() {
            $('.kup-magazyn__form-prenumerata').fadeOut();
            $('html').css({
                'overflow-y': 'scroll'
            });
        });
        $('.page-czytaj .cta-zamow').click(function() {
            jQuery('input[name="numer-prenumeraty"]').val('');
            $('.kup-magazyn__form-prenumerata').fadeIn();
            var nrpre = $(this).data('prenumerata');
            jQuery('input[name="numer-prenumeraty"]').val(nrpre);
            $('html').css({
                'overflow-y': 'hidden'
            });
        });
        $('.page-czytaj .cta-zamow .form-zamow-body-close').click(function() {
            $('.kup-magazyn__form-prenumerata').fadeOut();
            $('html').css({
                'overflow-y': 'scroll'
            });
        });


    });

    jQuery('.section-4 .row-faq_single h3').on('click', function() {
        jQuery('.row-faq_single').removeClass('active');
        jQuery(this).parent().addClass('active');
    })



    // Run the script once the window finishes loading
    $(window).load(function() {

    });

});