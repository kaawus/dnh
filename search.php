<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>


<section class="articles-header">
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <div class="header">
                    <h1>Wyszukiwarka</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<div class="results">
					<h2>Wyniki dla : <?php echo $s; ?></h2>
				</div>
                <div class="search-bar">
                <form action="/" method="get">
                    <input type="text" name="s" id="search" placeholder="Wyszukaj ..." value="<?php the_search_query(); ?>" />
                    <input type="submit" id="searchsubmit" value="Szukaj" /> 
                    <input type="hidden" value="post" name="post_type" id="post_type" />               </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="articles-grid">
    <div class="container">
        <div class="row">
        <?php 
        
		if ( have_posts() ) :


			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
                <div class="col-lg-4  articles-grid__single">
                    <a href="<?php the_permalink(); ?>" style="background: url(<?php the_post_thumbnail_url();?>); display: block; background-size: cover; background-position: center top;">
                    <!-- <img src="<?php the_post_thumbnail_url();?>" alt=""> -->
                    <div class="info-post">
                        <h3 class="title"><?php if ( $tytul_wpisu = get_field( 'tytul_wpisu' ) ) : ?>
                            <?php echo $tytul_wpisu; ?>
                            <?php endif; ?></h3>
                        <div class="author">
                            <?php
                                global $post;
                                $a_id=$post->post_author;
                                    $fname = get_the_author_meta('first_name', $a_id );
                                    $lname = get_the_author_meta('last_name', $a_id );
                                    echo $fname . ' <span>' . $lname . '</span>';
                                ?>

                        </div>
                        <div class="info-post__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                        </div>
                    </div>
                </a>
                </div>
            <?php endwhile;
       


        wp_reset_postdata();
		
		else :

			echo '<div class="no-results"><h3>Brak wyników dla ' . $s . '</h3></div>';

		endif;
		?>


        </div>
    </div>
</section>


<?php
get_footer();
