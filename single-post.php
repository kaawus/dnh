<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<section id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="hero-news"
            style="background: url('<?php the_post_thumbnail_url(); ?>'); background-size: cover; background-position: center;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="mt-0">
                            <?php if ( $tytul_wpisu = get_field( 'tytul_wpisu' ) ) : ?>
                            <?php echo $tytul_wpisu; ?>
                            <?php endif; ?>
                        </h1>
                        <?php if( $post->ID == 421) { ?>
                        <div class="autor" style="opacity: 0;pointer-events:none;">
                            <?php
							global $post;
							$a_id=$post->post_author;
								$fname = get_the_author_meta('first_name', $a_id );
								$lname = get_the_author_meta('last_name', $a_id );
								echo $fname . ' <span>' . $lname . '</span>';
							?>
                        </div>
                        <?php } else { ?>
                        <div class="autor">
                            <?php
							global $post;
							$a_id=$post->post_author;
								$fname = get_the_author_meta('first_name', $a_id );
								$lname = get_the_author_meta('last_name', $a_id );
								echo $fname . ' <span>' . $lname . '</span>';
							?>
                        </div>
                        <?php } ?>


                    </div>
                </div>
            </div>
        </div>
        <div class="flexible-content">
            <?php
		if ( have_rows( 'content' ) ) :
			while ( have_rows( 'content' ) ) :
				the_row(); ?>

            <?php if( get_row_layout() == 'zwykle_tekst' ) { ?>
            <div class="similar-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php the_sub_field('tekst1'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if( get_row_layout() == 'tekst_na_szarym_polu' ) { ?>
            <div class="similar-content-grey">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php the_sub_field('tekst2'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if( get_row_layout() == 'tekst_na_zoltym_tle' ) { ?>
            <div class="similar-content-yelow">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="yellow-content">
                                <?php the_sub_field('tekst3'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if( get_row_layout() == 'tekst_oraz_zdjecie' ) { ?>
            <div class="similar-content-image">
                <div class="container-fluid">
                    <div class="row">
                        <?php if ( get_sub_field( 'zdjecie_po_prawej' ) ) : ?>
                        <div class="col-lg-6 content content-left">
                            <?php the_sub_field('tekst4'); ?>
                        </div>
                        <div class="col-lg-6 image image-right">
                            <?php if ( $opis_nad_zdjeciem = get_sub_field( 'opis_nad_zdjeciem' ) ) : ?>
                            <div class="image-desc"><?php echo $opis_nad_zdjeciem; ?></div>
                            <?php endif; ?>
                            <img src="<?php echo esc_url( get_sub_field( 'zdjecie' ) ); ?>" alt="">
                            <?php if ( $zrodlo_zdjecie = get_sub_field( 'zrodlo_zdjecie' ) ) : ?>
                            <div class="zrodlo"><?php echo esc_html( $zrodlo_zdjecie ); ?></div>
                            <?php endif; ?>
                        </div>
                        <?php else: ?>
                        <div class="col-lg-6 image image-left">
                            <?php if ( $opis_nad_zdjeciem = get_sub_field( 'opis_nad_zdjeciem' ) ) : ?>
                            <div class="image-desc"><?php echo $opis_nad_zdjeciem; ?></div>
                            <?php endif; ?>
                            <img src="<?php echo esc_url( get_sub_field( 'zdjecie' ) ); ?>" alt="">
                            <?php if ( $zrodlo_zdjecie = get_sub_field( 'zrodlo_zdjecie' ) ) : ?>
                            <div class="zrodlo"><?php echo esc_html( $zrodlo_zdjecie ); ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6 content content-right">
                            <?php the_sub_field('tekst4'); ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php } ?>


            <?php endwhile;
		endif; ?>
        </div>
        <div class="autor-info">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="autor">
                            <?php
								global $post;
								$a_id=$post->post_author;
									$fname = get_the_author_meta('first_name', $a_id );
									$lname = get_the_author_meta('last_name', $a_id );
									echo $fname . ' <span>' . $lname . '</span>';
								?>

                        </div>
                        <div class="stanowisko">
                            <?php 
								$author_id = $post->post_author;
								$opis = get_field('pozycja_w_firmie', 'user_'. $author_id);
								echo $opis;
								?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="desc-autor">
                            <?php 
								
								the_field('opis_osoby', 'user_'. $author_id);
								?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();