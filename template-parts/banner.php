<?php
/**
 * Template part for banner
 *
 */

?>

<section class="banner">
    <div class="swiper mySwiperBanner">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="swiper-slide-content">
                    <div class="swiper-slide__image-left">
                        <img src="/wp-content/uploads/2022/01/lewa-str.jpg" alt="">
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-6 bannerfixpos">
                                <span>Już jest!</span>
                                <div class="number-magazine">nr<div>2</div>
                                </div>
                                <h1>Digital Now! Magazine</h1>
                                <div class="excerpt">Piszemy, jak skutecznie działać w świecie online marketingu i
                                    e-commerce</div>
                                <a href="/czytaj-online/#numer-2" class="cta">Pobierz nowy numer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="swiper-slide">
                <div class="swiper-slide-content">
                    <div class="swiper-slide__image-left">
                            <img src="/wp-content/uploads/2022/01/lewa-str.jpg" alt="">
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-6 offset-lg-6">
                                    <span>Juz jest!</span>
                                    <div class="number-magazine">nr<div>2</div></div>
                                    <h1>Digital Now! Magazine</h1>
                                    <div class="excerpt">Piszemy, jak skutecznie działać w świecie online marketingu i e-commerce</div>
                                    <a href="#" class="cta">Pobierz nowy numer</a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
-->


        </div>
        <!--<div class="swiper-arrows">
                <div class="swiper-next"><img src="/wp-content/uploads/2022/01/Ksztalt-6.svg" alt=""></div>
                <div class="swiper-prev"><img src="/wp-content/uploads/2022/01/Ksztalt-6.svg" alt=""></div>
            </div>-->
    </div>
</section>