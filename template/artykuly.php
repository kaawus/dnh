<?php
/**
* Template Name: Artykuły
 */

get_header(); ?>


<section class="articles-header">
    <div class="containers">
        <div class="row">
            <div class="col-lg-12">
                <div class="header">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="wp-terms">
                    <?php 
                    // $args = array( 'hide_empty=0' );
                    $terms = get_terms( 'kategorie', array( 'hide_empty' => false, 'parent' => 0 ) );
                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                        echo '<ul>';
                        foreach ( $terms as $term ) {
                            echo '<li><a href="' . get_term_link($term). '">' . $term->name . '</a></li>';
                        }
                        echo '</ul>';
                    }
                    ?>
                </div>
                <div class="search-bar">
                <form action="/" method="get">
                    <input type="text" name="s" id="search" placeholder="Wyszukaj ..." value="<?php the_search_query(); ?>" />
                    <input type="submit" id="searchsubmit" value="Szukaj" /> 
                    <input type="hidden" value="artyku" name="post_type" id="post_type" />               </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="articles-grid">
    <div class="container">
        <div class="row">
        <?php 
        $args_query = array(
            'post_type' => array('artyku'),
            'order' => 'DESC',
            'posts_per_page' => -1,
        );

        $query = new WP_Query( $args_query );

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post(); ?>
                <div class="col-lg-4  articles-grid__single">
                    <a href="<?php the_permalink(); ?>" style="background: url(<?php the_post_thumbnail_url();?>); display: block; background-size: cover; background-position: center top;">
                    <!-- <img src="<?php the_post_thumbnail_url();?>" alt=""> -->
                    <div class="info-post">
                        <h3 class="title"><?php if ( $tytul_wpisu = get_field( 'tytul_wpisu' ) ) : ?>
                            <?php echo $tytul_wpisu; ?>
                            <?php endif; ?></h3>
                        <div class="author">
                            <?php
                                global $post;
                                $a_id=$post->post_author;
                                    $fname = get_the_author_meta('first_name', $a_id );
                                    $lname = get_the_author_meta('last_name', $a_id );
                                    echo $fname . ' <span>' . $lname . '</span>';
                                ?>

                        </div>
                        <div class="info-post__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                        </div>
                    </div>
                </a>
                </div>
            <?php }
        } else {

        }

        wp_reset_postdata();?>


        </div>
    </div>
</section>



<?php
get_footer(); ?>