<?php
/**
* Template Name: Czytaj Online
 */

get_header(); ?>

<div class="page-czytaj">

    <section class="first">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php if ( $wprowadzenie = get_field( 'wprowadzenie' ) ) : ?>
                    <?php echo $wprowadzenie; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>



    <?php if ( have_rows( 'magazyn' ) ) : ?>
    <section class="magazyn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <?php while ( have_rows( 'magazyn' ) ) :
		the_row(); ?>

                    <div class="image-magazyn"
                        id="numer-<?php if ( $link_id = get_sub_field( 'link_id' ) ) : ?><?php echo esc_html( $link_id ); ?><?php endif; ?>">
                        <img src="<?php echo esc_url( get_sub_field( 'zdjecie_magazynu' ) ); ?>" alt=""
                            style="max-width: 100%; height: auto; margin: -10% auto -10% auto; display: block;">
                    </div>

                    <div class="magazyn-prenumerata">
                        <?php if ( $numer_magazynu = get_sub_field( 'numer_magazynu' ) ) : ?>
                        <div class="numer-magazynu"><?php echo esc_html( $numer_magazynu ); ?></div>
                        <?php endif; ?>

                        <?php if ( $naglowek_obok_numeru = get_sub_field( 'naglowek_obok_numeru' ) ) : ?>
                        <h3><?php echo $naglowek_obok_numeru; ?></h3>
                        <?php endif; ?>
                    </div>

                    <?php if ( $opis_pod_naglowkiem = get_sub_field( 'opis_pod_naglowkiem' ) ) : ?>
                    <div class="opis-prenumeraty">
                        <?php echo $opis_pod_naglowkiem; ?>
                    </div>
                    <?php endif; ?>

                    <?php if ( have_rows( 'spis_tresci' ) ) : ?>
                    <div class="spis-tresci">
                        <?php while ( have_rows( 'spis_tresci' ) ) :
                    the_row(); ?>

                        <div class="pojedyncze <?php if ( get_sub_field( 'ukryc' ) == 'Tak' ) : ?>hidden-pojedyncze<?php endif; ?>"
                            style="<?php if ( get_sub_field( 'ukryc' ) == 'Tak' ) : ?>display: none;<?php endif; ?>">

                            <?php if ( $naglowek_po_lewej = get_sub_field( 'naglowek_po_lewej' ) ) : ?>
                            <div class="naglowek"><?php echo $naglowek_po_lewej; ?></div>
                            <?php endif; ?>

                            <?php if ( $lista_po_prawej = get_sub_field( 'lista_po_prawej' ) ) : ?>
                            <div class="opis"><?php echo $lista_po_prawej; ?></div>
                            <?php endif; ?>
                        </div>

                        <?php endwhile; ?>
                        <div class="pokaz-caly-spis">Rozwiń</div>
                        <div class="chowaj-caly-spis" style="display:none">Zwiń</div>
                    </div>
                    <?php endif; ?>

                    
                    <div class="col-lg-12 offset-lg-0 cta-col text-center text-lg-left">
                        <div class="buttons">
                         <a id="nr-<?php if ( $link_id = get_sub_field( 'link_id' ) ) : ?><?php echo esc_html( $link_id ); ?><?php endif; ?>"
                            class="cta cta-form">Pobierz magazyn <img src="/wp-content/uploads/2022/01/arrow.svg" alt=""></a>
                            <a id="nr-<?php if ( $link_id = get_sub_field( 'link_id' ) ) : ?><?php echo esc_html( $link_id ); ?><?php endif; ?>"
                            class="cta cta-zamow" data-prenumerata="<?php if ( get_sub_field( 'link_id' ) == 1 )  { echo 'Prenumerata 01/21'; } else { echo 'Prenumerata 02/21'; } ?>">Zamów egzemplarz papierowy <img src="/wp-content/uploads/2022/01/arrow.svg" alt=""></a>
                        </div>
                        <div class="download-magazine">
                            <?php echo do_shortcode(get_sub_field('formularz_shortcode')); ?>

                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

</div>



<div class="kup-magazyn__form-prenumerata">
        <div class="kup-magazyn__form--prenumerata">
            <?php echo do_shortcode( '[contact-form-7 id="732" title="Formularz - Czytaj Online"]' );?>
        </div>
    </div>




<?php
get_footer(); ?>