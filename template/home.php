<?php
/**
* Template Name: Home
 */

get_header(); ?>


<?php get_template_part( 'template-parts/banner');?>


<section class="articles" id="articles">
    <div class="title">
        <h2 class="text-uppercase"><strong>A</strong>rtykuły</h2>
    </div>
    <div class="articles-wrapper">
        <div class="articles-wrapper__left">



            <?php 
        $args_query = array(
            'post_type' => array('artyku'),
            'order' => 'DESC',
            'posts_per_page' => 1,
        );

        $query = new WP_Query( $args_query );

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post(); ?>
            <a href="<?php the_permalink(); ?>">
                <img src="<?php the_post_thumbnail_url();?>" alt="">
                <div class="info-post">
                    <h3 class="title"><?php if ( $tytul_wpisu = get_field( 'tytul_wpisu' ) ) : ?>
                        <?php echo $tytul_wpisu; ?>
                        <?php endif; ?></h3>
                    <div class="author">
                        <?php
							global $post;
							$a_id=$post->post_author;
								$fname = get_the_author_meta('first_name', $a_id );
								$lname = get_the_author_meta('last_name', $a_id );
								echo $fname . ' <span>' . $lname . '</span>';
							?>

                    </div>
                    <div class="info-post__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                    </div>
                </div>
            </a>
            <?php }
        } else {

        }

        wp_reset_postdata();?>



        </div>
        <div class="articles-wrapper__right">
            <div class="articles-wrapper__right-top">
                <?php 
        $args_query = array(
            'post_type' => array('artyku'),
            'order' => 'DESC',
            'posts_per_page' => 2,
            'offset' => 1,
        );

        $query = new WP_Query( $args_query );

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post(); ?>
                <a href="<?php the_permalink(); ?>">
                    <img src="<?php the_post_thumbnail_url();?>" alt="">
                    <div class="info-post">
                        <h3 class="title"><?php if ( $tytul_wpisu = get_field( 'tytul_wpisu' ) ) : ?>
                            <?php echo $tytul_wpisu; ?>
                            <?php endif; ?></h3>
                        <div class="author">
                            <?php
							global $post;
							$a_id=$post->post_author;
								$fname = get_the_author_meta('first_name', $a_id );
								$lname = get_the_author_meta('last_name', $a_id );
								echo $fname . ' <span>' . $lname . '</span>';
							?>
                        </div>
                        <div class="info-post__readmore">Przeczytaj więcej<img
                                src="/wp-content/uploads/2022/01/arrow.svg"></div>

                    </div>
                </a>
                <?php }
        } else {

        }

        wp_reset_postdata();?>


            </div>
            <div class="articles-wrapper__right-bottom">
                <?php 
        $args_query = array(
            'post_type' => array('artyku'),
            'order' => 'DESC',
            'posts_per_page' => 1,
            'offset' => 3,
        );

        $query = new WP_Query( $args_query );

        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post(); ?>
                <a href="<?php the_permalink(); ?>">
                    <img src="<?php the_post_thumbnail_url();?>" alt="">
                    <div class="info-post">
                        <h3 class="title"><?php if ( $tytul_wpisu = get_field( 'tytul_wpisu' ) ) : ?>
                            <?php echo $tytul_wpisu; ?>
                            <?php endif; ?></h3>
                        <div class="author">
                            <?php
							global $post;
							$a_id=$post->post_author;
								$fname = get_the_author_meta('first_name', $a_id );
								$lname = get_the_author_meta('last_name', $a_id );
								echo $fname . ' <span>' . $lname . '</span>';
							?>
                        </div>
                        <div class="info-post__readmore">Przeczytaj więcej<img
                                src="/wp-content/uploads/2022/01/arrow.svg"></div>
                    </div>
                </a>
                <?php }
        } else {

        }

        wp_reset_postdata();?>
            </div>
        </div>
    </div>
</section>


<section class="magazyn">
    <div class="swiper mySwiperMagazyn">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="swiper-slide__content">
                    <div class="left">
                        <span class="black">
                            poznaj<br>nasz<br>magazyn!
                        </span>
                        <span class="yellow">nr<div>1</div></span>
                    </div>
                    <img src="/wp-content/uploads/2022/01/mag11-1.png" alt="">
                    <div class="right">
                        <span class="big"><strong>M</strong>AG</span>

                    </div>
                    <a href="/czytaj-online/#numer-1" class="cta">Czytaj online <img
                            src="/wp-content/uploads/2022/01/arrow.svg" alt=""></a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="swiper-slide__content">
                    <div class="left">
                        <span class="black">
                            poznaj<br>nasz<br>magazyn!
                        </span>
                        <span class="yellow">nr<div>2</div></span>
                    </div>
                    <img src="/wp-content/uploads/2022/01/mag22-2.png" alt="">
                    <div class="right">
                        <span class="big"><strong>M</strong>AG</span>

                    </div>
                    <a href="/czytaj-online/#numer-2" class="cta">Czytaj online <img
                            src="/wp-content/uploads/2022/01/arrow.svg" alt=""></a>
                </div>
            </div>
        </div>
        <div class="swiper-arrows">
            <div class="swiper-next-magazyn"><img src="/wp-content/uploads/2022/01/Ksztalt-6.svg" alt=""></div>
            <div class="swiper-prev-magazyn"><img src="/wp-content/uploads/2022/01/Ksztalt-6.svg" alt=""></div>
        </div>
    </div>
</section>

<section class="news" id="news">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase"> <strong>N</strong>ewsy</h2>
            </div>
        </div>
    </div>
    <div class="swiper mySwiperNews">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <a href="/sxo-maly-krokdla-seowca-wielki-krokw-strone-uzytecznosci/">
                    <img src="/wp-content/uploads/2022/01/Clip-26.png" alt="">
                    <div class="title">
                        <h3>SXO – mały krok dla SEOwca, wielki krok w stronę użyteczności
                        </h3>
                        <div class="title__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                        </div>
                    </div>

                </a>
            </div>
            <div class="swiper-slide">
                <a href="/core-web-vitals-‒-czym-jest-i-jak-moze-wplynac-na-twoja-pozycje-w-wyszukiwarce/">
                    <img src="/wp-content/uploads/2022/01/Clip-27.png" alt="">
                    <div class="title">
                        <h3>Core Web Vitals ‒ czym jest i jak może wpłynąć na Twoją pozycję w wyszukiwarce?</h3>
                        <div class="title__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                        </div>
                    </div>
                </a>
            </div>
            <div class="swiper-slide">
                <a href="/12-trendow-w-seow-2022-roku/">
                    <img src="/wp-content/uploads/2022/01/Clip-28.png" alt="">
                    <div class="title">
                        <h3>12 TRENDÓW W SEO W 2022 ROKU
                        </h3>
                        <div class="title__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                        </div>
                    </div>
                </a>
            </div>
            <div class="swiper-slide">
                <a href="/influence-marketing-w-e-commerce/">
                    <img src="/wp-content/uploads/2022/01/Clip-29.png" alt="">
                    <div class="title">
                        <h3>Influence
                            marketing
                            w e&#8209;commerce</h3>
                        <div class="title__readmore">Przeczytaj więcej<img src="/wp-content/uploads/2022/01/arrow.svg">
                        </div>
                    </div>

                </a>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>

<section class="newsletter">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>Zapisz się<span>do newslettera</span></h2>
            </div>
            <div class="col-lg-12 form-content">
                <?php echo do_shortcode('[contact-form-7 id="23" title="Formularz - Newsletter"]'); ?>
            </div>
        </div>
    </div>
</section>

<section class="education">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase"><strong>P</strong>rojekty<span><strong>E</strong>dukacyjne</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 single">
                <div class="single-content">
                    <a href="https://www.grupa-icea.pl/zapytaj-o-seo/" target="_blank">
                        <img src="/wp-content/uploads/2022/01/Clip.jpg" alt="">
                        <span class="cta">Sprawdź <img src="/wp-content/uploads/2022/01/arrow.svg" alt=""></span>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 single">
                <div class="single-content">
                    <a href="https://www.grupa-icea.pl/seo-newsy/" target="_blank">
                        <img src=" /wp-content/uploads/2022/01/seonews.png" alt="">
                        <span class="cta">Sprawdź <img src="/wp-content/uploads/2022/01/arrow.svg" alt=""></span>

                    </a>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="people">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase"><strong>P</strong>iszą <span>dla <strong>N</strong>as</span></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <!-- Swiper -->
                <div class="swiper mySwiperppl">
                    <div class="swiper-wrapper">
                        <?php if ( have_rows( 'osoby', '104' ) ) : ?>
                        <?php while ( have_rows( 'osoby', '104' ) ) :
                         the_row(); ?>
                        <div class="swiper-slide">
                            <div class="single">
                                <a href="/pisza-dla-nas/#<?php echo get_sub_field('link_hash_bez_spacji_itd');?>">
                                    <div class="single-person">
                                        <img src="<?php echo esc_url( get_sub_field( 'zdjecie' ) ); ?>" alt="">
                                        <div class="name">
                                            <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
                                            <?php echo $imie_i_nazwisko; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="swiper-arrows">
                    <div class="swiper-prev-person"><img src="/wp-content/uploads/2022/01/Ksztalt-6.svg" alt="">
                    </div>
                    <div class="swiper-next-person"><img src="/wp-content/uploads/2022/01/Ksztalt-6.svg" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<?php if ( get_field( 'ukryj_sekcja_parnterzy_home', 'options' ) == 'nie' ) : ?>
<section class="partners">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="text-uppercase"><strong>N</strong>asi <span><strong>P</strong>artnerzy</span></h2>
            </div>
        </div>
    </div>
    <div class="partners-carousel">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="swiper mySwiperLogo">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/crehler.png" alt="">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/BM-pion-blue-1.png"
                                        alt="">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/jash.png" alt="">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/smsapi_logo.png"
                                        alt="">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/booste.png" alt="">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/lh.png" alt="">
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="logo-holder">
                                    <img src="https://magazyndigital.pl/wp-content/uploads/2022/01/zaufane-pl-1.png"
                                        alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?php endif; ?>

<script>
var swiper = new Swiper(".mySwiperppl", {
		slidesPerView: 1,
		grid: {
			rows: 2,
		},
		spaceBetween: 30,
		breakpoints: {

			480: {
				slidesPerView: 2,
			},
			992: {
				slidesPerView: 3,
			}
		},
		navigation: {
			nextEl: ".swiper-next-person",
			prevEl: ".swiper-prev-person",
		},
	      autoResize: false,
        resizeReInit: true,
	});



</script>


<?php
get_footer(); ?>