<?php
/**
* Template Name: Kup magazyn
 */

get_header(); ?>



<div class="kup-magazyn">

    <div class="hero">
        <h1><span>Kup /</span> Magazyn</h1>
    </div>

    <div class="buy-section">
        <div class="buy-section__left">
            <div class="buy-section__left--title2">
                <p><span>4 wydania</span><br>
                    drukowane w roku</p>
                <p><span>Każde wydanie to 100 stron</span><br>
                    wypełnionych merytoryką i praktycznymi poradami</p>
                <p><span>Dostęp</span><br>
                    do archiwalnych wydań online</p>
                <p><span>darmowa</span><br>
                    wysyłka</p>
                <p><span>W prenumeracie taniej </span><br>
                    – za jeden numer płacisz 29,75 zł zamiast 39 zł!</p>
                <span class="cena">
                    CENA <strong>119 ZŁ</strong>
                </span>
                <div class="buy-section__left--cta2">
                    KUP TERAZ
                </div>
            </div>
            <div class="buy-section__left--title1">
                prenumerata<br>
                <span>drukowana</span><br>
                na rok
            </div>
            <div class="buy-section__left--cta1">
                Więcej
            </div>
            <img src="/wp-content/uploads/2022/02/kup-left.png" alt="">
        </div>
        <div class="buy-section__right">
            <div class="buy-section__right--title2">
                <p><span>Wydanie minimum 100 stron</span>
                    wypełnionych merytoryką
                    i praktycznymi poradami</p>
                <p><span>darmowa</span><br>
                    wysyłka</p>
                <span class="cena">
                    CENA <strong>39 ZŁ</strong>
                </span>
                <div class="buy-section__right--cta2">
                    KUP TERAZ
                </div>
            </div>
            <div class="buy-section__right--title1">
                kup<br>
                pojedynczy<br>
                numer<br>
                <span>drukowany</span>
            </div>
            <div class="buy-section__right--cta1">
                Więcej
            </div>
            <img src="/wp-content/uploads/2022/02/kup-right.png" alt="">
        </div>
    </div>

    <div class="selected-articles">
        <div class="selected-articles__content">
            <h2>Wybrane artykuły</h2>
            <div class="selected-aricles__content-loop">
                <?php 
                                // Custom WP query query
                $args_query = array(
                    'post_type' => array('artyku'),
                    'order' => 'DESC',
                );

                $query = new WP_Query( $args_query );

                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post(); ?>
                <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
                <?php }
                } else {

                }

                wp_reset_postdata();
                ?>

            </div>

        </div>
    </div>

    <div class="why-learning">
        <div class="why-learning__content">
            <div class="why-learning__content-left">
                <h2>Dlaczego warto czytać<br><span>magazyn digital</span></h2>
                <ul>
                    <li>Uzyskasz dostęp do unikatowej wiedzy ekspertów</li>
                    <li>Będziesz na bieżąco z trendami w e-commerce i marketingu oraz będziesz umieć wykorzystać je w
                        codziennych działaniach</li>
                    <li>Poznasz nowe ścieżki rozwoju biznesu e-commerce</li>
                    <li>Dowiesz się, jakie działania marketingowe przekładają się na większą sprzedaż</li>
                    <li>Będziesz świadomy aktualnych potrzeb i oczekiwań klientów</li>
                    <li>Nauczysz się sprawdzonych praktyk, dzięki którym zbudujesz wartościową markę na rynku</li>
                </ul>
            </div>
            <div class="why-learning__content-right">
                <img src="https://magazyndigital.pl/wp-content/uploads/2022/02/kompozycja.png" alt="">
            </div>
        </div>
    </div>

    <div class="kup-magazyn__form-prenumerata">
        <div class="kup-magazyn__form--prenumerata">
            <?php echo do_shortcode( '[contact-form-7 id="728" title="Formularz wyslij prenumerata"]' );?>
        </div>
    </div>
    <div class="kup-magazyn__form-prenumerata2">
        <div class="kup-magazyn__form--prenumerata2">
            <?php echo do_shortcode( '[contact-form-7 id="729" title="Formularz wyslij - pojedyncza prenumerata"]' );?>
        </div>
    </div>

</div>


<?php
get_footer(); ?>