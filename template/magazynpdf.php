<?php
/**
* Template Name: Magazyn PDF
 */

get_header(); ?>

<div class="magazyn-czytaj-online"
    style="min-height: 90vh; padding-top: 120rem; background: #000; padding-bottom: 50px;">

    <?php the_content();?>

</div>

<style>
.magazyn-czytaj-online .ipgs-theme-dark {
    background: #000;
}
</style>

<?php
get_footer(); ?>