<?php
/**
* Template Name: O magazynie 2
 */

get_header(); ?>

<div class="page-magazyn">


<section class="banner-page">
    <div class="container-fluid">
        <div class="row align-items-lg-center">
            <div class="col-lg-12">
                <div class="banner-page__left">
                <?php if ( $lewa = get_field( 'lewa' ) ) : ?>
	<?php echo $lewa; ?>
<?php endif; ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="banner-page__right">
                <?php if ( $prawa = get_field( 'prawa' ) ) : ?>
	<?php echo $prawa; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-1__left text-lg-right">
                <?php if ( $opis_po_lewej = get_field( 'opis_po_lewej' ) ) : ?>
	<?php echo $opis_po_lewej; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-1-image">
    <?php
$zdjecie_po_prawej = get_field( 'zdjecie_po_prawej' );
$size = 'full';
if ( $zdjecie_po_prawej ) {
	$url = wp_get_attachment_url( $zdjecie_po_prawej );
	echo wp_get_attachment_image( $zdjecie_po_prawej, $size );
}; ?>
    </div>
</section>

<section class="section-2">
    <div class="container-fluid">
    <div class="row align-items-lg-center">
            <div class="col-lg-6">
                <div class="section-2__left">
                <?php if ( $napis_po_lewej = get_field( 'napis_po_lewej' ) ) : ?>
<h2>	<?php echo esc_html( $napis_po_lewej ); ?></h2>
<?php endif; ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="section-2__right">
                <?php if ( $opis_po_prawej = get_field( 'opis_po_prawej' ) ) : ?>
	<?php echo $opis_po_prawej; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


</div>


<div class="page-piszaonas">

    <div class="banner-piszaonas">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="col-content">
                        <h1><span>Piszą</span> dla nas</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="osoby">
        <div class="container-fluid">
            <div class="row">
                <?php if ( have_rows( 'osoby', 104 ) ) : ?>
                <?php while ( have_rows( 'osoby', 104 ) ) :
                the_row(); ?>

                <div class="col-lg-3 col-md-6">
                    <div class="osoba-single"
                        id="<?php if ( $link_hash_bez_spacji_itd = get_sub_field( 'link_hash_bez_spacji_itd' ) ) : ?><?php echo esc_html( $link_hash_bez_spacji_itd ); ?><?php endif; ?>">
                        <div class="osoba-single_square">
                            <img src="<?php echo esc_url( get_sub_field( 'zdjecie' ) ); ?>" alt="">
                            <div class="name">
                                <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
                                <?php echo $imie_i_nazwisko; ?>
                                <?php endif; ?>
                            </div>




                        </div>

                    </div>
                    <div class="desc-osoba" style="display:none;">
                        <div class="desc-osoba__content">
                            <div class="desc-osoba__content--name">
                                <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
                                <?php echo $imie_i_nazwisko; ?>
                                <?php endif; ?>
                            </div>
                            <div class="desc-osoba__content--position">
                                <?php if ( $pozycja = get_sub_field( 'pozycja' ) ) : ?>
                                <?php echo $pozycja; ?>
                                <?php endif; ?>
                            </div>
                            <div class="desc-osoba__content--bio">
                                <?php if ( $opis = get_sub_field( 'opis' ) ) : ?>
                                <?php echo $opis; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>



                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--<div class="osoba-single-desc">
        <div class="osoba-single-desc__contaniner">
            <div class="osoba-single-desc__contaniner--top">
                <div class="osoba-single-desc__contaniner--top__name">
                    Jacek <span>Dziura</span>
                </div>
                <div class="osoba-single-desc__contaniner--top__position">
                    red. naczelny, Członek Zarządu, <br>
                    Chief Marketing Officer w Grupa iCEA
                </div>
            </div>
            <div class="osoba-single-desc__contaniner--bottom">
                Jacek buduje silne marki oraz efektywne strategie w holdingu Digital Now!.
                Wspiera rozwój sieci partnerskiej i dba o edukację klientów. W codziennej pracy
                przekłada doświadczenie zdobyte w budowaniu globalnych marek. Praktyk strategii
                i skutecznego budowania brandów. W pracy pasjonuje się ujęciem
                klientocentrycznym i zawsze patrzy przez pryzmat konsumenta – zarówno
                w marketingu, jak i w eCommerce. W wolnych chwilach wychowuje młodego dobermana
                i boksuje.
            </div>
        </div>
    </div>-->
</div>


<div class="page-redakcja">

<div class="banner-redakcja">
    <div class="container-fluid">
        <div class="content-banner">
            <h1><span>RED</span>AKCJA</h1>
        </div>
    </div>
</div>



<?php if ( have_rows( 'osoby', 83 ) ) : ?>
	<?php while ( have_rows( 'osoby', 83 ) ) :
		the_row(); ?>
		
	



        <div class="single-person">
    <div class="container-fluid">
        <div class="rows">
       <div class="image">
       <?php
		$zdjecie = get_sub_field( 'zdjecie' );
		$size = 'full';
		if ( $zdjecie ) {
			$url = wp_get_attachment_url( $zdjecie );
			echo wp_get_attachment_image( $zdjecie, $size );
		}; ?>
       </div>
            <div class="head-content">
               <div class="content">
                <div class="name">
                <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
			<?php echo $imie_i_nazwisko; ?>
		<?php endif; ?>
                </div>
                    <div class="small-opis">
                       
		<?php if ( $stanowisko = get_sub_field( 'stanowisko' ) ) : ?>
			<?php echo $stanowisko; ?>
		<?php endif; ?>
                    </div>
                    <div class="opis-glowny">
                    <?php if ( $opis = get_sub_field( 'opis' ) ) : ?>
			<?php echo $opis; ?>
		<?php endif; ?>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>

	<?php endwhile; ?>
<?php endif; ?>


<?php if ( $wydawca = get_field( 'wydawca', 83 ) ) : ?>

<div class="wydawca">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
	<?php echo $wydawca; ?>

            </div>
        </div>
    </div>
    <div class="element">
        <img src="/wp-content/uploads/2022/01/kula.png" alt="">
    </div>
</div>
<?php endif; ?>


</div>

<?php
get_footer(); ?>