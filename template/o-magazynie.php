<?php
/**
* Template Name: O magazynie
 */

get_header(); ?>

<div class="page-magazyn">


<section class="banner-page">
    <div class="container-fluid">
        <div class="row align-items-lg-center">
            <div class="col-lg-6">
                <div class="banner-page__left">
                <?php if ( $lewa = get_field( 'lewa' ) ) : ?>
	<?php echo $lewa; ?>
<?php endif; ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="banner-page__right">
                <?php if ( $prawa = get_field( 'prawa' ) ) : ?>
	<?php echo $prawa; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section-1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-1__left text-lg-right">
                <?php if ( $opis_po_lewej = get_field( 'opis_po_lewej' ) ) : ?>
	<?php echo $opis_po_lewej; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-1-image">
    <?php
$zdjecie_po_prawej = get_field( 'zdjecie_po_prawej' );
$size = 'full';
if ( $zdjecie_po_prawej ) {
	$url = wp_get_attachment_url( $zdjecie_po_prawej );
	echo wp_get_attachment_image( $zdjecie_po_prawej, $size );
}; ?>
    </div>
</section>

<section class="section-2">
    <div class="container-fluid">
    <div class="row align-items-lg-center">
            <div class="col-lg-6">
                <div class="section-2__left">
                <?php if ( $napis_po_lewej = get_field( 'napis_po_lewej' ) ) : ?>
<h2>	<?php echo esc_html( $napis_po_lewej ); ?></h2>
<?php endif; ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="section-2__right">
                <?php if ( $opis_po_prawej = get_field( 'opis_po_prawej' ) ) : ?>
	<?php echo $opis_po_prawej; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


</div>


<?php
get_footer(); ?>