<?php
/**
* Template Name: Piszą o nas
 */

get_header(); ?>

<div class="page-piszaonas">

    <div class="banner-piszaonas">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="col-content">
                        <h1><span>Piszą</span> dla nas</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2>Autorzy artykułów Digital Now! Magazine</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="osoby">
        <div class="container-fluid">
            <div class="row">
                <?php if ( have_rows( 'osoby' ) ) : ?>
                <?php while ( have_rows( 'osoby' ) ) :
                the_row(); ?>

                <div class="col-lg-3 col-md-6">
                    <div class="osoba-single"
                        id="<?php if ( $link_hash_bez_spacji_itd = get_sub_field( 'link_hash_bez_spacji_itd' ) ) : ?><?php echo esc_html( $link_hash_bez_spacji_itd ); ?><?php endif; ?>">
                        <div class="osoba-single_square">
                            <img src="<?php echo esc_url( get_sub_field( 'zdjecie' ) ); ?>" alt="">
                            <div class="name">
                                <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
                                <?php echo $imie_i_nazwisko; ?>
                                <?php endif; ?>
                            </div>




                        </div>

                    </div>
                    <div class="desc-osoba" style="display:none;">
                        <div class="desc-osoba__content">
                            <div class="desc-osoba__content--name">
                                <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
                                <?php echo $imie_i_nazwisko; ?>
                                <?php endif; ?>
                            </div>
                            <div class="desc-osoba__content--position">
                                <?php if ( $pozycja = get_sub_field( 'pozycja' ) ) : ?>
                                <?php echo $pozycja; ?>
                                <?php endif; ?>
                            </div>
                            <div class="desc-osoba__content--bio">
                                <?php if ( $opis = get_sub_field( 'opis' ) ) : ?>
                                <?php echo $opis; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>



                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--<div class="osoba-single-desc">
        <div class="osoba-single-desc__contaniner">
            <div class="osoba-single-desc__contaniner--top">
                <div class="osoba-single-desc__contaniner--top__name">
                    Jacek <span>Dziura</span>
                </div>
                <div class="osoba-single-desc__contaniner--top__position">
                    red. naczelny, Członek Zarządu, <br>
                    Chief Marketing Officer w Grupa iCEA
                </div>
            </div>
            <div class="osoba-single-desc__contaniner--bottom">
                Jacek buduje silne marki oraz efektywne strategie w holdingu Digital Now!.
                Wspiera rozwój sieci partnerskiej i dba o edukację klientów. W codziennej pracy
                przekłada doświadczenie zdobyte w budowaniu globalnych marek. Praktyk strategii
                i skutecznego budowania brandów. W pracy pasjonuje się ujęciem
                klientocentrycznym i zawsze patrzy przez pryzmat konsumenta – zarówno
                w marketingu, jak i w eCommerce. W wolnych chwilach wychowuje młodego dobermana
                i boksuje.
            </div>
        </div>
    </div>-->
</div>


<?php
get_footer(); ?>