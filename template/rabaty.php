<?php
/**
* Template Name: Rabaty
 */

get_header(); ?>

<div class="page-rabaty">

    <div class="banner-rabaty">
        <div class="container-fluid">
            <div class="row align-items-lg-center">
                <div class="col-lg-6">
                    <h1>Rabaty</h1>
                </div>
                <div class="col-lg-6 desc">
                    <p>Nikt nie lubi przepłacać! Dlatego właśnie wraz z naszymi partnerami, którzy wspierają biznesy
                        działające online przygotowaliśmy pakiet rabatów dla każdego marketera
                        i&#8239;przedsiębiorcy. Wszystkie zniżki pozwolą Ci zaoszczędzić
                        na&#8239;usługach i produktach, które wspomagają prowadzenie efektywnych działań marketingowych.
                        Sprawdź,
                        ile&#8239;możesz&#8239;zaoszczędzić i zoptymalizuj koszty już dziś!
                        <strong class="text-uppercase d-block">Sprawdź jak działają rabaty w 3 krokach:</strong>
                    </p>
                    <ul>
                        <li>Zobacz aktualnie dostępne oferty rabatowe.</li>
                        <li>Zapoznaj się opisem i przekonaj się w jaki sposób można skorzystać ze zniżki.</li>
                        <li>Sprawdź warunki, zakup usługę lub produkt
                            z&#8239;rabatem i ciesz się oszczędnościami!</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>


    <div class="logotypy">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <?php if ( have_rows( 'rabaty_pole_powtarzalne_lewa' ) ) : ?>
                    <?php while ( have_rows( 'rabaty_pole_powtarzalne_lewa' ) ) :
                        the_row(); ?>
                    <div
                        class="single <?php if ( get_sub_field( 'pole_podwojne' ) == 'tak' ) : ?>single-double<?php endif; ?>">
                        <div class="logo">
                            <?php
                        $logo = get_sub_field( 'logo' );
                        if ( $logo ) : ?>
                            <img src="<?php echo esc_url( $logo['url'] ); ?>"
                                alt="<?php echo esc_attr( $logo['alt'] ); ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="big"><?php if ( $rabat = get_sub_field( 'rabat' ) ) : ?>
                            <?php echo esc_html( $rabat ); ?>
                            <?php endif; ?></div>
                        <div class="desc"> <?php if ( $opis = get_sub_field( 'opis' ) ) : ?>
                            <?php echo $opis; ?>
                            <?php endif; ?></div>
                    </div>


                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="col-lg-6">

                    <?php if ( have_rows( 'rabaty_pole_powtarzalne_prawa' ) ) : ?>
                    <?php while ( have_rows( 'rabaty_pole_powtarzalne_prawa' ) ) :
                        the_row(); ?>
                    <div
                        class="single <?php if ( get_sub_field( 'pole_podwojne' ) == 'tak' ) : ?>single-double<?php endif; ?>">
                        <div class="logo">
                            <?php
                        $logo = get_sub_field( 'logo' );
                        if ( $logo ) : ?>
                            <img src="<?php echo esc_url( $logo['url'] ); ?>"
                                alt="<?php echo esc_attr( $logo['alt'] ); ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="big"><?php if ( $rabat = get_sub_field( 'rabat' ) ) : ?>
                            <?php echo esc_html( $rabat ); ?>
                            <?php endif; ?></div>
                        <div class="desc"> <?php if ( $opis = get_sub_field( 'opis' ) ) : ?>
                            <?php echo $opis; ?>
                            <?php endif; ?></div>
                    </div>


                    <?php endwhile; ?>
                    <?php endif; ?>



                </div>
            </div>
        </div>
    </div>

</div>


<?php
get_footer(); ?>