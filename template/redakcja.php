<?php
/**
* Template Name: Redakcja
 */

get_header(); ?>

<div class="page-redakcja">

<div class="banner-redakcja">
    <div class="container-fluid">
        <div class="content-banner">
            <h1><span>RED</span>AKCJA</h1>
        </div>
    </div>
</div>



<?php if ( have_rows( 'osoby' ) ) : ?>
	<?php while ( have_rows( 'osoby' ) ) :
		the_row(); ?>
		
	



        <div class="single-person">
    <div class="container-fluid">
        <div class="rows">
       <div class="image">
       <?php
		$zdjecie = get_sub_field( 'zdjecie' );
		$size = 'full';
		if ( $zdjecie ) {
			$url = wp_get_attachment_url( $zdjecie );
			echo wp_get_attachment_image( $zdjecie, $size );
		}; ?>
       </div>
            <div class="head-content">
               <div class="content">
                <div class="name">
                <?php if ( $imie_i_nazwisko = get_sub_field( 'imie_i_nazwisko' ) ) : ?>
			<?php echo $imie_i_nazwisko; ?>
		<?php endif; ?>
                </div>
                    <div class="small-opis">
                       
		<?php if ( $stanowisko = get_sub_field( 'stanowisko' ) ) : ?>
			<?php echo $stanowisko; ?>
		<?php endif; ?>
                    </div>
                    <div class="opis-glowny">
                    <?php if ( $opis = get_sub_field( 'opis' ) ) : ?>
			<?php echo $opis; ?>
		<?php endif; ?>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>

	<?php endwhile; ?>
<?php endif; ?>


<?php if ( $wydawca = get_field( 'wydawca' ) ) : ?>

<div class="wydawca">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
	<?php echo $wydawca; ?>

            </div>
        </div>
    </div>
    <div class="element">
        <img src="/wp-content/uploads/2022/01/kula.png" alt="">
    </div>
</div>
<?php endif; ?>


</div>


<?php
get_footer(); ?>