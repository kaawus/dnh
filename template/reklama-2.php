<?php
/**
* Template Name: Reklama 2
 */

get_header(); ?>

<div class="page-reklama">


<section class="banner-page">
    <div class="container-fluid">
        <div class="row align-items-lg-center">
           <div class="col-lg-12">
           <?php if ( $hero_content = get_field( 'hero_content' ) ) : ?>
	<?php echo $hero_content; ?>
<?php endif; ?>
           </div>
        </div>
    </div>
</section>

<section class="section-1">
    <div class="section-1__image">
    <?php
$zdjecie_2 = get_field( 'zdjecie_2' );
$size = 'full';
if ( $zdjecie_2 ) {
	$url = wp_get_attachment_url( $zdjecie_2 );
	echo wp_get_attachment_image( $zdjecie_2, $size, "", ["class" => "image-1"]);
}; ?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 offset-lg-6">
                <div class="section-1__content">
                <?php if ( $opis_2 = get_field( 'opis_2' ) ) : ?>
	<?php echo $opis_2; ?>
<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-1-1" style="background: url(https://magazyndigital.pl/wp-content/uploads/2022/02/Grupa-5.jpg); ">
    <div class="container-fluid">
        <div class="row">
            <div class="row-content">
            <strong>Szukasz nowego kanału dotarcia do swojej grupy docelowej?</strong> 
            <p>My to możemy Ci zapewnić. Za naszym Magazynem stoi spółka, którą zrzesza kilkanaście podmiotów, które świadczą usługi w dziedzinie reklamy internetowej, wspierania sklepów e-commerce oraz obsługi widoczności stron internetowych. Dzięki temu masz pewność, że Twoja reklama trafi do osób zainteresowanych.</p>
            </div>
        </div>
    </div>
</section>

<section class="section-1-2" style="background: url(https://magazyndigital.pl/wp-content/uploads/2022/02/AdobeStock_139272988-min.jpg); ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <h2>Jak możemy<br>współpracować?</h2>
            </div>
            <div class="content-on-bilbord">
                <ul>
                <li>Reklama wizualna w wydaniu drukowanym</li>
                <li>Artykuł sponsorowany w wydaniu<br>drukowanym oraz wersji online</li>
                <li>Artykuł sponsorowany w naszym serwisie online</li>
                <li>Insert dołączony do wydania drukowanego</li>
                <li>Może masz jeszcze jakiś pomysł,<br>w jakiej formule widzisz naszą współpracę?</li>

                </ul>
            </div>
        </div>
    </div>
</section>


<section class="section-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7">
                <div class="content">
                <?php if ( $opis_3 = get_field( 'opis_3' ) ) : ?>
	<?php echo $opis_3; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="section-3" style="background: url(https://magazyndigital.pl/wp-content/uploads/2022/02/tlo2.jpg); ">
    <div class="container-fluid">
        <div class="row">
            <div class="row-form">
                <?php echo do_shortcode('[contact-form-7 id="697" title="Formularz kontaktowy - reklama"]'); ?>
            </div>
        </div>
    </div>
</section>

<section class="section-4" style="background: url(/wp-content/uploads/2022/02/tlofaq.png);background-position: center top!important;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>FAQ</h2>
            </div>
        </div>
        <div class="row row-faq">
            <div class="col-lg-12 row-faq_single active">
                <h3>Dla kogo powstał Magazyn Digital?</h3>
                <div class="row-faq_single-desc">Magazyn Digital to czasopismo skierowane do marketerów, przedsiębiorców oraz właścicieli biznesów e-commerce. Jeżeli jednak nie pracujesz branży, a interesujesz się tą tematyką, to możesz być pewny, że to Magazyn również dla Ciebie.</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>Jakie tematy poruszane są na łamach Magazynu Digital?</h3>
                <div class="row-faq_single-desc">W czasopiśmie znajdują się artykuły poradnikowe poświęcone marketingowi online, a w szczególności takim zagadnieniom jak m.in. e-commerce, SEO, SEM, social media, content marketing, analityka.

</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>W jakiej formie dostępne jest czasopismo?</h3>
                <div class="row-faq_single-desc">Magazyn Digital dostępny jest w formie drukowanej oraz elektronicznej (PDF).

</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>Jak zamówić prenumeratę?</h3>
                <div class="row-faq_single-desc">Roczna prenumerata Magazynu Digital to koszt 119 zł. Zdecydowanie bardziej opłaca się zakup prenumeraty – wówczas za jeden numer zapłacisz tylko 29,75 zł zamiast 39 zł. 

</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>Ile kosztuje prenumerata?</h3>
                <div class="row-faq_single-desc">Roczna prenumerata Magazynu Digital to koszt 119 zł. Zdecydowanie bardziej opłaca się zakup prenumeraty – wówczas za jeden numer zapłacisz tylko 29,75 zł zamiast 39 zł. 

</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>Czy prenumerata roczna przedłuża się automatycznie na kolejne miesiące?</h3>
                <div class="row-faq_single-desc">Absolutnie nie. Kupujesz abonament na rok, podczas którego otrzymasz 4 numery drukowane. Jeżeli będziesz zainteresowany kontynuowaniem prenumeraty, wówczas przypomnimy Ci się pod koniec okresu rozliczeniowego. 

</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>Ile kosztuje pojedynczy egzemplarz?</h3>
                <div class="row-faq_single-desc">Pojedynczy egzemplarz Magazynu Digital to koszt 39 zł.</div>
            </div>
            <div class="col-lg-12 row-faq_single">
                <h3>Jak mogę skontaktować się z redakcją?</h3>
                <div class="row-faq_single-desc">Wyślij swoje zapytanie na adres: <a href="mailto:redakcja@magazyndigital.pl">redakcja@magazyndigital.pl</a>. Jesteśmy otwarci na pytania oraz sugestie, zapraszamy do kontaktu :)</div>
            </div>
        </div>
    </div>
</section>




<?php
get_footer(); ?>