<?php
/**
* Template Name: Reklama
 */

get_header(); ?>

<div class="page-reklama">


<section class="banner-page">
    <div class="container-fluid">
        <div class="row align-items-lg-center">
           <div class="col-lg-12">
           <?php if ( $hero_content = get_field( 'hero_content' ) ) : ?>
	<?php echo $hero_content; ?>
<?php endif; ?>
           </div>
        </div>
    </div>
</section>

<section class="section-1">
    <div class="section-1__image">
    <?php
$zdjecie_2 = get_field( 'zdjecie_2' );
$size = 'full';
if ( $zdjecie_2 ) {
	$url = wp_get_attachment_url( $zdjecie_2 );
	echo wp_get_attachment_image( $zdjecie_2, $size, "", ["class" => "image-1"]);
}; ?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 offset-lg-6">
                <div class="section-1__content">
                <?php if ( $opis_2 = get_field( 'opis_2' ) ) : ?>
	<?php echo $opis_2; ?>
<?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>


<section class="section-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7">
                <div class="content">
                <?php if ( $opis_3 = get_field( 'opis_3' ) ) : ?>
	<?php echo $opis_3; ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer(); ?>